#!/usr/bin/env python
"""
get-spect.py - Given a URL, this will retrieve all of the fits files found at
    that URL and download them into the cache if they are not downloaded already.
"""

from hashlib import blake2b
from pathlib import Path
from urllib.request import urlretrieve
from urllib.parse import urlparse

from requests_html import HTMLSession


def getLinks(url):
    session = HTMLSession()
    r = session.get(url)
    return r.html.absolute_links


def main(url):
    h = blake2b(key=bytearray(url, 'utf-8'), digest_size=12)
    cache_dir = Path('./cache/' + h.hexdigest())
    Path.mkdir(cache_dir, parents=True, exist_ok=True)

    for link in getLinks(url):
        if link.endswith('.fit'):
            link_parsed = urlparse(link)
            link_path = Path(link_parsed.path)
            file_name = link_path.name
            file_path = cache_dir / file_name
            if not Path(file_path).exists():
                print(f'Saving {url} as {file_path}')
                try:
                    urlretrieve(link, file_path)
                except:
                    print(f'ERROR: failed to retrieve {link}')


if __name__ == "__main__":
    url='http://www.astrosurf.com/aras/Aras_DataBase/Symbiotics/RAqr.htm'
    main(url)
