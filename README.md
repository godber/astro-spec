# astro-spec

```
conda env create -f environment.yml
# get the fit files
python ./get-spect.py

# generate plots from fit files
python ./gen-spect.py
```

Right now, this code only grabs all the FITs files from this specific page:


http://www.astrosurf.com/aras/Aras_DataBase/Symbiotics/RAqr.htm

It currently plots the first observation in the table "20/11/2010"


![20/11/2010](example/asdb_raqr_20101120_841.png "20/11/2010")

The plot from the ASTROSURF site is here:

http://www.astrosurf.com/aras/Aras_DataBase/Symbiotics/FitFiles/Images/asdb_raqr_20101120_841.png