#!/usr/bin/env python

from pathlib import Path

from astropy.io import fits
from astropy import units as u
from astropy.visualization import quantity_support

import matplotlib.pyplot as plt
import numpy as np

# quantity_support()


def plot(in_file_path, out_dir=Path('out/')):
    in_file = in_file_path.stem
    Path.mkdir(out_dir, parents=True, exist_ok=True)
    out_file = out_dir / Path(in_file + '.png')
    print(out_file)

    f = fits.open(in_file_path)
    data = f[0].data
    f.close()

    fig, ax = plt.subplots(1, figsize=(11, 8))
    ax.plot(data)
    ax.set_title(in_file)
    ax.set_xlabel('X label')
    ax.set_ylabel('Y label')
    fig.savefig(out_file)


def main(dir):
    for fit_file in Path(dir).glob('*.fit'):
        plot(fit_file)


if __name__ == "__main__":
    main('cache/b0761876272400dcf118bb1f/')